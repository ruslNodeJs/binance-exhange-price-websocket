const express = require('express');
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
const BinanceExchangeWebsocket = require('./helpers/binanceWebSocket')
const addInstance = require('./helpers/addInstance')
const binanceController = require('./binance/controllers/binanceController')

  const app = express();
  let port = 5000;
  
  app.get('/getBinanceUsdtTokenPairs', async (req, res) => {
    return res.send(await binanceController.getTokenPairs())
  });
  app.post('/addWebsocketToken', async (req, res) => {
    await binanceController.addWebsocketTokens(req,res)
  });
  app.delete('/deleteWebsocketToken', async (req, res) => {
    await binanceController.deleteWebsocketTokens(req , res)
  });
  app.get('/BinanceExchangeRate', async (req, res) => {
    BinanceExchangeWebsocket(req,res)
  });
  app.post('/addInstances', async (req, res) => {
    addInstance(req,res)
  });

  app.listen(port, () => {
    console.log(`App listening on ${port} in process ${process.pid}`);
  });