export { };
import * as WebSocket from 'websocket';
const pgp = require('pg-promise')();
const db = require('../db');
module.exports = async (req, res) => {
    const wsTokens: string[] = []
    const tokenList = db.any(`SELECT name FROM tokens`)
    const wsList = db.any(`SELECT name FROM exchange_rates`)
    for (let i in wsList) {
        wsTokens.push(wsList[i].name)
    }

    for (let i in tokenList) {
        let tokenName = tokenList[i].name
        console.log(tokenName)
        console.log(wsTokens)

        let binanceWSUrl = `wss://stream.binance.com:9443/ws/${tokenName}usdt@trade`;
        const wsClient = new WebSocket.client();

        wsClient.on('connect', (connection: WebSocket.connection) => {
            connection.on('message', async (message: WebSocket.IMessage) => {
                if (message.type === 'utf8') {
                    const data = JSON.parse(message.utf8Data);
                    if(wsTokens.includes(tokenName.toUpperCase())){
                        await db.any(`UPDATE exchange_rates SET price='${data.p} WHERE name=${data.s}')`)
                    } else {
                        await db.any(`INSERT INTO exchange_rates(name,price) VALUES('${data.s}' , '${data.p}')`)
                    }
                }
            });
            connection.on('error', (error: Error) => {
                console.error(`WebSocket Error: ${error}`);
            });
        });
        wsClient.connect(binanceWSUrl);
    }
};

