export {}
const getTokenPairs = require("../services/getTokenPairs");
const getWebsocketTokens = require("../services/getWebsocketTokens");
const addWebsocketTokens = require("../services/addWebsocketTokens")
const deleteWebsocketTokens = require("../services/deleteWebsocketTokens")

class BinanceController {
    getTokenPairs = async(req,res) => getTokenPairs(req,res)
    getWebsocketTokens = async(req,res) => getWebsocketTokens(req,res) // method for check tokens which rate updates right now
    addWebsocketTokens = async(req,res) => addWebsocketTokens(req,res) 
    deleteWebsocketTokens = async(req,res) => deleteWebsocketTokens(req,res) 
}

const binanceController = new BinanceController()
module.exports = binanceController