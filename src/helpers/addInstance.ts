export { };
const cluster = require('cluster');
const BinanceExchangeWebsocket = require('./binanceWebSocket')

module.exports = async (req, res) => {
    if (cluster.isMaster) {
        for (let i = 0; i < req.query.numProcces; i++) {
            cluster.fork();
        }

        cluster.on('exit', (worker, code, signal) => {
            console.log(`Procces ${worker.process.pid} finished`);
        });
    } else {
       BinanceExchangeWebsocket()
       res.send(`Websocket ${process.pid} instance started!`)
    }
};



