export {};
import axios from 'axios';

const baseUrl = 'https://api.binance.com/api/v3';

module.exports = async (req,res) => {
        try {
            const response = await axios.get(`${baseUrl}/exchangeInfo`);
            const currencyPairs = response.data.symbols.map((symbol: any) => symbol.symbol);
            return currencyPairs
        } catch (error) {
            console.error('Error while responding:', error);
            throw error;
        }
};